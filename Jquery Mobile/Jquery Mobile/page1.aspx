﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="page1.aspx.cs" Inherits="Jquery_Mobile.page1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta name="viewport" content="user-scalable=no,width-device-width" />
    <link rel="stylesheet" href="jquery.mobile/jquery.mobile.css" />
    <script src="jquery.js"></script>
    <script src="jquery.mobile/jquery.mobile.js"></script>


    <title></title>
</head>
<body>
    
   <div data-role="page" id="pageone">
  <div data-role="header">
    <h1>Insert Page Title Here</h1>
  </div>
       </div>
       
  <div data-role="main" class="ui-content">
    <p>Insert Content Here</p>
  </div>

  <div data-role="footer">
    <h1>Insert Footer Text Here</h1>
  </div>
  
    

</body>
</html>
